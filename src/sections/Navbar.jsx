// Navbar.js
import React from 'react';
import '../styles/Navbar.css';
import { FaInstagram, FaWhatsapp } from 'react-icons/fa';
import { motion } from 'framer-motion';

const Navbar = () => {
  const scrollToSection = (sectionId, event) => {
    event.preventDefault();
    const section = document.getElementById(sectionId);
    if (section) {
      section.scrollIntoView({ behavior: 'smooth' });
    }
  };

  return (
    <nav className="navbar">
      <motion.nav
        className="navbar-one"
        whileHover={{ backgroundColor: 'rgba(255, 0, 0, 1)', transition: { duration: 0.5 } }}
      >
        <div className="navbar-one">
          <img src="./logo.png" alt="Logo" className="logo" />
          <div className="logo-text">
            <span>E-Learn</span>
            <span>E-nglish</span>
          </div>
        </div>
      </motion.nav>
      <div className="navbar-three">
        <ul className="nav-links" style={{ fontSize: '1.5vw' }}>
          <li>
            <a href="#inicio" onClick={(e) => scrollToSection('inicio', e)}>Inicio</a>
          </li>
          <li>
            <a href="#sobre-mi" onClick={(e) => scrollToSection('sobre-mi', e)}>Sobre mí</a>
          </li>
          <li>
            <a href="#clases-grupales" onClick={(e) => scrollToSection('clases-grupales', e)}>Clases</a>
          </li>
          <li>
            <a href="#testimonios" onClick={(e) => scrollToSection('testimonios', e)}>Testimonios</a>
          </li>
          <li>
            <a href="#contacto" onClick={(e) => scrollToSection('contacto', e)}>Contacto</a>
          </li>
        </ul>
      </div>
      <div className="navbar-two">
        <div className='social-icons-container'>
          <a href="https://wa.me/1131442552" target="_blank" rel="noopener noreferrer">
            <FaWhatsapp className="social-icon" style={{ color: '#25d366' }} />
          </a>
          <a href="https://www.superprof.com.ar/clases-ingles-personalizadas-pedido-conversacion-apoyo-escolar-universitario-laboral-preparacion-para-entrevistas-laborales.html" target="_blank" rel="noopener noreferrer">
            <img src="./images/superprof.png" alt="Superprof" className="social-icon" />
          </a>
          <a href="link-a-instagram" target="_blank" rel="noopener noreferrer">
            <FaInstagram className="social-icon" style={{ color: '#dd2a7b' }} />
          </a>
        </div>
      </div>
      <div className='navbar-four'>
        <button className="language-button">ES/EN</button>
      </div>
    </nav>
  );
};

export default Navbar;
